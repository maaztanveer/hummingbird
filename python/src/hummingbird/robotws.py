# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2016, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Subscribes to a robot's websocket stream.
    NOTE: Requires websocket-client to be installed.
    See https://github.com/websocket-client/websocket-client
"""

import json
import math
import sys
import thread
import time
import websocket


class RobotWebsocket(object):
    """ Robot websocket helper class. """
    def __init__(self, uri):
        self.uri = uri
        self.ws = websocket.WebSocketApp(uri, on_message=self._on_message, on_error=self._on_error, on_close=self._on_close)
        self.ws.on_open = self._on_open
        self.on_pose = None
        self.on_error = None

    def start(self):
        """ Starts data streaming. """
        self.ws.run_forever()

    def stop(self):
        """ Stops streaming."""
        self.ws.close()

    def single(self):
        """ Query and return a single message. """
        self.ws.send('{"op":"subscribe","topic":"/slam/pose"}')
        message = self.ws.recv()
        raw = json.loads(message)
        spatial = self._parse_pose(raw)
        pose = self._convert_spatial(spatial)
        return pose

    def _on_message(self, ws, message):
        """ Invoked by websocket when data is available. """
        if self.on_pose:
            raw = json.loads(message)
            spatial = self._parse_pose(raw)
            pose = self._convert_spatial(spatial)
            self.on_pose(pose)

    def _on_error(self, ws, error):
        """ Invoked by websocket on error. """
        if self.on_error:
            self.on_error(error)

    def _on_close(self, ws):
        """ Invoked by websocket on close. """
        pass

    def _on_open(self, ws):
        """ Invoked by websocket on opening. """
        def run(*args):
            self.ws.send('{"op":"subscribe","topic":"/slam/pose"}')
        thread.start_new_thread(run, ())

    def _parse_pose(self, jsondata):
        """ Parses json data into xyz and quaternion. """
        x = jsondata["msg"]["pose"]["pose"]["position"]["x"]
        y = jsondata["msg"]["pose"]["pose"]["position"]["y"]
        z = jsondata["msg"]["pose"]["pose"]["position"]["z"]
        a = jsondata["msg"]["pose"]["pose"]["orientation"]["w"]
        b = jsondata["msg"]["pose"]["pose"]["orientation"]["x"]
        c = jsondata["msg"]["pose"]["pose"]["orientation"]["y"]
        d = jsondata["msg"]["pose"]["pose"]["orientation"]["z"]
        return {"x": x, "y": y, "z": z, "a": a, "b": b, "c": c, "d": d}

    def _convert_spatial(self, spatial):
        t3 = +2.0 * (spatial["a"] * spatial["d"] + spatial["b"] * spatial["c"])
        t4 = +1.0 - 2.0 * (spatial["c"] * spatial["c"] + spatial["d"] * spatial["d"])
        yaw = math.atan2(t3, t4) / math.pi * 180
        return (spatial["x"], spatial["y"], yaw)
