# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2016, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import rospy
import threading
from .rest import RestProxy as API


class Mission(object):
    """ A NIMBUS mission. """
    def __init__(self, description=""):
        self.tasks = []
        self.description = description
        self.finalized = True
        self.priority = 0
        self.force_robot = None
        self.assigned_robot = None
        self.paused = False
        self.poll_sleep = 2.0  # seconds
        self.on_done = None
        self.on_cancelled = None
        self.on_succeeded = None
        self.on_assigned = None
        self.on_executing = None
        self.on_failed = None
        self.on_paused = None
        self.on_queued = None
        self.on_new_task = None
        self.on_new_step = None
        self.on_change = None
        self.on_heartbeat = None
        self.worker = threading.Thread(target=self._monitor)
        self.lock = threading.Lock()
        self.stop = False
        self.autostop = True  # Stop monitoring when done.

    def add_task(self, task):
        """ Adds a task to the mission. """
        self.tasks.append(task.__dict__)

    def add_tasks(self, tasks):
        """ Adds tasks to the mission. """
        for task in tasks:
            self.tasks.append(task.__dict__)

    def enqueue(self):
        """ Enqueues the mission. """
        result = API.post("missions", self)
        self.__dict__.update(result)
        self.worker.name = "Monitoring " + self.id

    def monitor(self):
        """ Enables monitoring of this mission. """
        self.worker.start()

    def unmonitor(self):
        """ Disables monitoring of this mission. """
        self.stop = True

    def _monitor(self):
        """ Monitors the mission status by polling the REST API. """
        callbacks = {"ASSIGNED": "on_assigned", "CANCELLED": "on_cancelled", "EXECUTING": "on_executing",
                     "FAILED": "on_failed", "PAUSED": "on_paused", "QUEUED": "on_queued",
                     "SUCCEEDED": "on_succeeded"}
        done_callbacks = ["CANCELLED", "FAILED", "SUCCEEDED"]
        last_status = None
        last_task = None
        last_step = None

        while not self.stop:
            if self.on_heartbeat:
                self.on_heartbeat(self)

            result = API.get("missions", self.id)
            status = result.mission_status
            if status != last_status:
                with self.lock:
                    self.__dict__.update(result)
                callback = self.__dict__[callbacks[status]]
                if callback:
                    callback(self)
                if self.on_change:
                    self.on_change(self)
                if status in done_callbacks:
                    if self.on_done:
                        self.on_done(self)
                    if self.autostop:
                        return
                last_status = status
            task = result.current_task
            if task != last_task and self.on_new_task:
                with self.lock:
                    self.__dict__.update(result)
                self.on_new_task(self)
                if self.on_change:
                    self.on_change(self)
                last_task = task

            try:
                rospy.sleep(self.poll_sleep)
            except KeyboardInterrupt:
                return
        self.stop = False

    def is_monitoring(self):
        """ Returns true if monitoring is active. """
        return self.worker.is_alive()

    def wait(self, timeout=None):
        """ Waits for the mission to complete synchronously. """
        if not self.is_monitoring():
            self.monitor()
        self.worker.join(timeout)

    def __repr__(self):
        id = self.id if "id" in self.__dict__ else None
        return "Mission {0}: {1} ({2} tasks)".format(id, self.description, len(self.tasks))

    @classmethod
    def query(cls, query=None):
        """ Enumerates all missions that meet the specified query.  """
        missions = API.get("missions")
        return [m for m in missions if query(m)] if query and missions else missions

