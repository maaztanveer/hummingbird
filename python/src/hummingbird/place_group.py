# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2018, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from .lookup import Lookup


class PlaceGroup(object):
    """ A place group. """
    def __init__(self, clone={}):
        self.id = None
        self.created = None
        self.name = None
        self.description = None
        self.places = None
        if clone:
            self.__dict__.update(clone)

    def __repr__(self):
        return "PlaceGroup {0}: {1}".format(self.id, self.name)

    @classmethod
    def find(cls, id):
        """ Gets a place group by its id. """
        place_group = Lookup.a_by_b("place_groups", "id", id)
        return PlaceGroup(place_group) if place_group else None

    @classmethod
    def find_by_name(cls, name):
        """ Gets a place group by its name. """
        place_group = Lookup.a_by_b("place_groups", "name", name)
        return PlaceGroup(place_group) if place_group else None
