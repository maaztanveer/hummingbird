# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2016, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Module: RestProxy class
RestProxy is a static class providing a simplified/abstracted means to exchange data with
a RESTful HTTP interface. Once it has been initialized with the URI of the interface, the
get - post - put - delete methods can be invoked with the name of an endpoint to issue
API requests.

* If invoked by a ROS node, the ROS logging facility will be used.
* If the ``REST_API_URI`` environment variable is set, it will be used as the default API URI.
* Using ``initialize`` will override the defauly API URI.
* Experts: Set ``default_serializer`` to change the default JSON serializer.

Usage::

    >>> from rest import RestProxy as API
    >>> API.initialize("http://example.com/api/v1/")
    >>> if not API.post("endpoint", {id: 23, name="Joe"}):
    >>>     print "Error posting to endpoint."

Options::
    >>> API.raise_exceptions = False  # Raise a RestException if unsuccessful.
    >>> API.print_errors = False  # Print/log response if unsuccessful.
    >>> API.quiet = True  # Suppresses verbose debugging messages.
"""

import dotdict
import json
import os
import requests
import sys

# Use rospy logging facility if it is available, and a node is running.
try:
    import rospy
    has_rospy = rospy.core.is_initialized()
except ImportError:
    has_rospy = False


class RestProxy(object):
    """ A proxy to perform API calls to the RESTful interface. """
    headers = {"cache-control": "no-cache", "content-type": "application/json"}
    uri = os.environ["REST_API_URI"] if "REST_API_URI" in os.environ else None
    quiet = True
    print_errors = False  # Logs HTTP responses if unsuccessful.

    @classmethod
    def initialize(cls, uri):
        """ Invoke before using RestProxy.
        @param uri: (str) The URI of the REST API (excluding endpoint).
        """
        cls.uri = uri

    @classmethod
    def post(cls, api, data):
        """ POSTs the JSON data to the server using the REST interface.
        @param  api: (str) API endpoint
        @param data: (json) The json object to put
        @return:     (json) The result data if successful, otherwise
                     ((int, str)) The response if ``verbose_errors`` is set, otherwise None.
        """
        cls._check_initialized()
        url = "{0}{1}/".format(cls.uri, api)
        cls._logdebug("POST " + url)
        d = data if type(data) is dict else data.__dict__
        serialized = json.dumps(d, skipkeys=True, default=RestProxy.default_serializer)
        if not cls.quiet:
            print serialized
        response = requests.request("POST", url, data=serialized, headers=cls.headers)
        return cls._return(response)

    @classmethod
    def get(cls, api, id=None):
        """ GETs the JSON data to the server using the REST interface.
        @param api: (str) API endpoint
        @param  id: (str) The id of the object to get, or None to get all
        @return:    (json) The result data if successful, otherwise None
        """
        cls._check_initialized()
        url = "{0}{1}/".format(cls.uri, api)
        if id:
            url += id + "/"
        cls._logdebug("GET " + url)
        response = requests.request("GET", url, headers=cls.headers)
        return cls._return(response)

    @classmethod
    def put(cls, api, id, data):
        """ PUTs the JSON data to the server using the REST interface.
        @param  api: (str) API endpoint
        @param   id: (str) The id of the object to update
        @param data: (json) The json object to put
        @return:     (json) The result data if successful, otherwise None
        """
        cls._check_initialized()
        url = "{0}{1}/{2}/".format(cls.uri, api, id) if id else "{0}{1}/".format(cls.uri, api)
        cls._logdebug("PUT " + url)
        serialized = json.dumps(data, skipkeys=True, default=RestProxy.default_serializer)
        if not cls.quiet:
            print cls._log_debug(serialized)
        response = requests.request("PUT", url, data=serialized, headers=cls.headers)
        return cls._return(response)

    @classmethod
    def delete(cls, api, id):
        """ DELETEs the record with the specified ID from the server using the REST interface.
        @param api: (str) API endpoint
        @param  id: (str) The id of the object to delete
        @return:    (json) The result data if successful, otherwise None
        """
        cls._check_initialized()
        url = "{0}{1}/{2}/".format(cls.uri, api, id)
        cls._logdebug("DELETE " + url)
        response = requests.request("DELETE", url, headers=cls.headers)
        return cls._return(response)

    @classmethod
    def _check_initialized(cls):
        """ Checks that the API class has been initialized with the API URI. """
        if not cls.uri:
            cls._logerr("Call RestProxy.initialize() with the API URL before making REST calls.")
            sys.exit(100)
        # Add a trailing slash if not present.
        if cls.uri[-1:] != "/":
            cls.uri += "/"

    @classmethod
    def _return(cls, response):
        """ Utility method to return response in requested form. """
        if response.status_code not in range(200, 300):
            if cls.print_errors:
                cls._logerr("{0}: {1} returned {2}: {3}".format(response.request.method, response.url, response.status_code, response.reason + ": " + response.text))
            raise RestException(response)
        if not cls.quiet:
            cls._logdebug("{0} successful.".format(response.request.method))
        return json.loads(response.text, cls=dotdict.DotDictJSONDecoder) if response.text else True

    @classmethod
    def _logdebug(cls, message):
        if not cls.quiet:
            if has_rospy:
                rospy._logdebug(message)
            else:
                print "[DEBUG]: " + unicode(message)

    @classmethod
    def _loginfo(cls, message):
        if has_rospy:
            rospy._loginfo(message)
        else:
            print "[INFO]: " + unicode(message)

    @classmethod
    def _logwarn(cls, message):
        if has_rospy:
            rospy._logwarn(message)
        else:
            print "[WARNING]: " + unicode(message)

    @classmethod
    def _logerr(cls, message):
        if has_rospy:
            rospy._logerr(message)
        else:
            print "[ERROR]: " + unicode(message)

    @staticmethod
    def default_serializer(value):
        return ""


class RestException(Exception):
    """ An exception that occurred during a REST transaction. """
    def __init__(self, response):
        self.response = response
