# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2016, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import json


class DotDict(dict):
    """ Inherit this class to make dictionary members accessible using dot notation. """
    def __init__(self, clone={}):
        self._merge(clone)

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def _merge(self, clone):
        if clone:
            for key in clone:
                setattr(self, key, clone[key])


class DotDictJSONDecoder(json.JSONDecoder):
    """ Custom JSONDecoder to convert dict to DotDict during decoding. """
    def __init__(self, **kwargs):
        hook = self.hook
        json.JSONDecoder.__init__(self, object_hook=hook, **kwargs)

    def hook(self, obj):
        """ Converts the supplied object to a DotDict if it is a dict. """
        if isinstance(obj, dict):
            return DotDict(obj)
        return obj
