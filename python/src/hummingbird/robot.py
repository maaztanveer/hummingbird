# Software License Agreement (New BSD License)
#
# @author    Yvan Rodrigues <yrodrigues@clearpathrobotics.com>
# @copyright (c) 2016, Clearpath Robotics Inc., All rights reserved.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Clearpath Robotics BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import rospy
from .payload import Payload
from .rest import RestProxy as API
from .updateable import Updateable


class Robot(Updateable):
    """ A robot. """
    has_payload_callbacks = dict()

    def __init__(self, clone={}):
        self.id = None
        self.battery = None
        self.robot_type = None
        self.robot_status = None
        self.execution_state = None
        self.created = None
        self.name = None
        self.namespace = None
        self.hostname = None
        self.description = None
        self.last_startup = None
        self.appliance_ip = None
        self.loaded_map = None
        self.fallback_recipe = None
        self.team = None
        self.payload = None
        self.place = None
        self._merge(clone)
        # Not implemented by API, but useful to integrators.
        self.last_place = None  # The robot's last known place.
        self.next_place = None  # The robot's planned next place.
        self.charge_pending = False  # True if the robot is waiting to charge.
        self.payload_verified = False  # True if payload state has been verified using sensors.
        self.paused = False

    def update(self):
        """ Update this object's data from the API. """
        Updateable.update(self, "robots")
        if not self.payload_verified:
            self.verify_payload()

    def get_state(self):
        """ Gets the robot's current state. """
        return self.robot_status.system_state, self.robot_status.sub_system_state

    def get_battery_level(self):
        """ Gets the robot's current charge, as a percentage of full. """
        if not self.battery:
            rospy.logwarn("Robot '{0}' state of charge unavailable.".format(self.name))
            return 100
        current = float(self.battery.remaining_charge_capacity)
        full = float(self.battery.full_charge_capacity)
        return current / full * 100

    def has_payload(self):
        """ Returns True if the specified robot has a payload. Override this for your robot model. """
        # Execute robot-specific payload checks.
        model = self.robot_type.model_name
        if model in Robot.has_payload_callbacks.keys():
            payload = Robot.has_payload_callbacks[model](self)
            rospy.logdebug("Payload check for {0} returned {1}.".format(model, payload))
            return payload

        # Default handler.
        return self.payload

    def verify_payload(self):
        """ Set robot's payload based on sensor data, not database voodoo. """
        # Check sensor for payload.
        present = self.has_payload()
        if present is None:
            rospy.logwarn("Unable to sense if robot '{0}' has a payload.".format(self.name))
            return

        self.payload_verified = True
        rospy.logdebug("Payload present? Sensor says {0}, Nimbus says {1}".format(present, self.payload))

        # The real payload state matches what Nimbus thinks.
        if (present and self.payload) or (not present and not self.payload):
            rospy.logdebug("The sensed payload state matches what Nimbus thinks.")
            return

        # Correct Nimbus to match robot.
        payload = Payload.create()
        operation = {"operation": "SET_PAYLOAD", "robot": self.id, "arguments": {"payload": payload.id if present else None}}
        API.post("robots/operations", operation)
        self.payload = payload
        rospy.loginfo("Robot '{0}' payload has been updated to {1}.".format(self.name, payload.id))

    def is_charging(self):
        """ Returns true if the robot is charging. """
        return self.robot_status.sub_system_state == "CHARGING" or (self.battery and self.battery.charging_status == "CHARGING")

    def is_paused(self):
        """ Returns true if the robot is paused. """
        return self.robot_status.sub_system_state == "PAUSED"

    def is_offline(self):
        """ Returns true if the robot is offline. """
        return self.robot_status.sub_system_state == "OFFLINE"

    def is_idle(self):
        """ Returns true if the robot is idle. """
        return self.robot_status.sub_system_state == "STARVED"

    def __repr__(self):
        return "Robot {0}: {1}".format(self.id, self.name)

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    @classmethod
    def query(cls, query=None):
        """ Enumerates all robots that meet the specified query.  """
        robots = [Robot(r) for r in API.get("robots")]
        return [r for r in robots if query(r)] if query and robots else robots

    @classmethod
    def query_ids(cls, query=None):
        """ Enumerates all robot IDs. """
        robots = cls.robots(query)
        return [r.id for r in robots]

