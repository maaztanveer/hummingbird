from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup


d = generate_distutils_setup(
    packages=["hummingbird"],
    package_dir={'': 'src'}
)

setup(**d)

"""
from setuptools import setup

setup(name="hummingbird",
      version="1.0",
      description="Helpers for using the Nimbus API",
      url="http://bitbucket.org/clearpathrobotics/hummingbird",
      author="Yvan Rodrigues",
      author_email="yrodrigues@clearpath.ai",
      license="BSD",
      packages=["src/hummingbird"],
      install_requires=[
          "requests"
      ],
      zip_safe=False)
      
"""
