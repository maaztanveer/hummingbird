""" Deletes a robot by directly deleting it from the database.
    THIS IS NOT A SUPPORTED WORKFLOW. The offical solution is to
    flush your db and let the robots re-enumerate themselves.
"""


import psycopg2
import sys


# Constants.
DBHOST = "localhost"
DBNAME = "vault_db"
DBUSER = "vault_owner"
DBPASS = "vault_owner"
ROBOT_TABLES = ["vault_robotbatterystate",
                "vault_robotexception",
                "vault_robotexecutionstate",
                "vault_robotmapstatus",
                "vault_robotownershipbid",
                "vault_payload"
               ]

connection_string = "host={0} dbname={1} user={2} password={3}".format(DBHOST, DBNAME, DBUSER, DBPASS)

# Parse command line.
if len(sys.argv) < 2:
    print "Usage: python delete_robot <robot_name>"
    sys.exit(1)
robot_name = sys.argv[1]

# Connect to database.
with psycopg2.connect(connection_string) as connection:
    connection.autocommit = False
    with connection.cursor() as cursor:

        # Find the robot in vault_robots, if it exists. 
        print "Querying vault_robots."
        cursor.execute("SELECT id FROM vault_robot WHERE name = %s OR namespace = %s", (robot_name, robot_name))
        rows = cursor.fetchall()

        if len(rows) == 0:
            print "There aren't any robots by that name. Check your capitalization."
            sys.exit(3)

        if len(rows) > 1:
            print "There is more than one robot with that name or namespace. I can't help you with that."
            sys.exit(2)

        robot_id = str(rows[0][0])
        print "The id for robot '{0}' is {1}".format(robot_name, robot_id)
        # Delete all references for list of tables.
        for table in ROBOT_TABLES:
            print "Deleting any rows from {0}.".format(table)
            cursor.execute("DELETE FROM {0} WHERE robot_id = '{1}'".format(table, robot_id))

        # Delete from system state.
        cursor.execute("SELECT id FROM vault_robotstatus WHERE robot_id = '{0}'".format(robot_id))
        rows = cursor.fetchall()
        if len(rows) > 0:
            for row in rows:
                status_id = row[0]
                cursor.execute("DELETE FROM vault_robotstatustosystemstate WHERE robot_status_id = '{0}'".format(status_id))
            cursor.execute("DELETE FROM vault_robotstatus WHERE id = '{0}'".format(status_id))
        cursor.execute("DELETE FROM vault_robot WHERE id = '{0}'".format(robot_id))
        connection.commit()
    # Clean up.
    cursor.close()
connection.close()
