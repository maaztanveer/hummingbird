from cpppo.server.enip.get_attribute import proxy

def robot_has_payload(ip):
    """ Returns True if the specified robot has a payload. """
    plc = proxy(ip, route_path=False, send_path='', timeout=10)
    plc.list_identity()
    tag = "VO_FUL"
    print "Checking for payload"
    data, = plc.read(tag)
    present = bool(data[0])
    print "Robot {0} a pallet.".format("does have" if present else "does not have")
    return data

print robot_has_payload("10.134.8.211")
