import sys
from hummingbird import RobotWebsocket


# Parse command line.
if len(sys.argv) < 2:
    print "Usage: python socket.py <uri>"
    print "       uri is in the format ws://hostname:port"
    sys.exit(1)
uri = sys.argv[1]

# Define on_pose callback.
def print_message(pose):
    x, y, yaw = pose
    print "{0: >+7.2f}m      {1: >+7.2f}m       {2: >+7.3f}deg             \r".format(x, y, yaw),

# Create and subscribe.
robotws = RobotWebsocket(uri)
robotws.on_pose = print_message
robotws.start()
