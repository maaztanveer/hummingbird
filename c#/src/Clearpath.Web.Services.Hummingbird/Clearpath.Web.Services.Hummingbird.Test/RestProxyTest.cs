﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clearpath.Web.Services.Hummingbird;

namespace Clearpath.Web.Services.Hummingbird.Test
{
    [TestClass]
    public class RestProxyTest
    {
        /// <summary>
        /// Tests robot enumeration.
        /// </summary>
        [TestMethod]
        public void GetRobots()
        {
            // Initialize proxy singleton.
            const string apiUri = "http://prod-vm-nimbus-otto-01:8000/api/v1";
            //const string apiUri = "http://10.25.10.86:8000/api/v1";
            RestProxy.Instance.ApiUri = apiUri;

            var robots = Lookup.Instance.Robots();
            if (robots.Length == 0)
                Assert.Fail("No robots were enumerated.");
            var robot = Lookup.Instance.Robot("robot-ph-cpe18-43");
        }

        /// <summary>
        /// Tests mission enumeration.
        /// </summary>
        [TestMethod]
        public void GetMissions()
        {
            // Initialize proxy singleton.
            // const string apiUri = "http://prod-vm-nimbus-otto-01:8000/api/v1";
            const string apiUri = "http://dev-vm-nimbus-floor-01:8000/api/v1";
            RestProxy.Instance.ApiUri = apiUri;

            var missions = Lookup.Instance.Missions();
            if (missions.Length == 0)
                Assert.Fail("No missions were enumerated.");
        }


        /// <summary>
        /// Tests mission creation and place lookup.
        /// </summary>
        [TestMethod]
        public void CreateMission()
        {
            const string apiUri = "http://10.25.10.86:8000/api/v1";
            RestProxy.Instance.ApiUri = apiUri;

            var place = Lookup.Instance.Place("ORANGE");
            var mission = new Mission("Happy fun mission");
            var task = new Task(TaskType.Move, place.Id.Value);
            mission.Tasks.Add(task);
            mission.Enqueue();
        }

        /// <summary>
        /// Gets place enumeration.
        /// </summary>
        [TestMethod]
        public void GetPlaces()
        {
            const string apiUri = "http://10.25.10.86:8000/api/v1";
            RestProxy.Instance.ApiUri = apiUri;

            var places = Lookup.Instance.Places();
            if (places.Length == 0)
                Assert.Fail("No places were enumerated.");
        }
    }
}
