﻿using System;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Hummingbird Exception.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class HummingbirdException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HummingbirdException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public HummingbirdException(string message=null) : base(message)
        {
        }
    }
}
