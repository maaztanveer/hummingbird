﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// REST API proxy
    /// </summary>
    public class RestProxy
    {
        #region Fields
        private string _apiUri;
        #endregion

        #region Initialization
        /// <summary>
        /// Initializes static members of the <see cref="RestProxy"/> class.
        /// </summary>
        static RestProxy()
        {
            Instance = new RestProxy();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestProxy"/> class.
        /// </summary>
        public RestProxy()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestProxy"/> class.
        /// </summary>
        /// <param name="apiUri">The API URI.</param>
        public RestProxy(string apiUri)
        {
            ApiUri = apiUri;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <value>The instance.</value>
        public static RestProxy Instance { get; private set; }

        /// <summary>
        /// Gets or sets the API URI.
        /// </summary>
        /// <value>The API URI.</value>
        public string ApiUri
        {
            get { return _apiUri; }
            set
            {
                _apiUri = value;
                if (!_apiUri.EndsWith("/"))
                    _apiUri += "/";
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets data from the specified endpoint.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="api">The api.</param>
        /// <param name="queries">The queries.</param>
        /// <returns>The requested object(s).</returns>
        public T[] Get<T>(string api, params KeyValuePair<string,string>[] queries)
        {
            CheckInitialized();
            var uri = ApiUri + api + queries.Aggregate("/?", (s, p) => s += p.Key + "=" + p.Value);
            var req = HttpWebRequest.Create(uri);
            req.Method = "GET";
            using (var res = req.GetResponse())
            {
                return EvaluateResultMany<T>(res as HttpWebResponse);
            }
        }

        /// <summary>
        /// Posts the object to the specified API.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="api">The API.</param>
        /// <param name="value">The value.</param>
        /// <returns>The response object.</returns>
        public T Post<T>(string api, object value)
        {
            CheckInitialized();
            var uri = ApiUri + api + "/";
            var req = HttpWebRequest.Create(uri);
            req.Method = "POST";
            req.ContentType = "application/json";
            var stream = req.GetRequestStream();

            var js = new JsonSerializer();
            js.NullValueHandling = NullValueHandling.Ignore;
            using (var sw = new StreamWriter(stream))
            using (var jsw = new JsonTextWriter(sw))
            {
                js.Serialize(jsw, value);
                sw.Flush();
            }
            using (var res = req.GetResponse())
            {
                return EvaluateResultOne<T>(res as HttpWebResponse);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Checks if the API has been initialized.
        /// </summary>
        private void CheckInitialized()
        {
            // TODO
        }

        /// <summary>
        /// Evaluates the result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response">The response.</param>
        /// <returns>T.</returns>
        private T[] EvaluateResultMany<T>(HttpWebResponse response)
        {
            if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
                throw new RestException(response.StatusDescription, response);

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            //var data = sr.ReadToEnd();

            var jtr = new JsonTextReader(sr);
            var js = new JsonSerializer();
            js.NullValueHandling = NullValueHandling.Ignore;
            return js.Deserialize<T[]>(jtr);
        }

        /// <summary>
        /// Evaluates the result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response">The response.</param>
        /// <returns>T.</returns>
        private T EvaluateResultOne<T>(HttpWebResponse response)
        {
            if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
                throw new RestException(response.StatusDescription, response);

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            //var data = sr.ReadToEnd();

            var jtr = new JsonTextReader(sr);
            var js = new JsonSerializer();
            js.NullValueHandling = NullValueHandling.Ignore;
            return js.Deserialize<T>(jtr);
        }
        #endregion
    }
}
