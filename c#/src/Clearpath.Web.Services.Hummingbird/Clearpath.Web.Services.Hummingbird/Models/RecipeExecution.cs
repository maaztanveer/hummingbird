﻿using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Recipe Execution
    /// </summary>
    public class RecipeExecution
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="RecipeExecution"/> is paused.
        /// </summary>
        /// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
        [JsonProperty("paused")]
        public bool Paused { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [force queue].
        /// </summary>
        /// <value><c>true</c> if [force queue]; otherwise, <c>false</c>.</value>
        [JsonProperty("force_queue")]
        public bool ForceQueue { get; private set; }

        /// <summary>
        /// Gets or sets the queue context identifier.
        /// </summary>
        /// <value>The queue context identifier.</value>
        [JsonProperty("queue_context_id")]
        public string QueueContextID { get; private set; }

        /// <summary>
        /// Gets or sets the active job identifier.
        /// </summary>
        /// <value>The active job identifier.</value>
        [JsonProperty("active_job_id")]
        public string ActiveJobID { get; private set; }

        /// <summary>
        /// Gets or sets the distance to goal.
        /// </summary>
        /// <value>The distance to goal.</value>
        [JsonProperty("distance_to_goal")]
        public double DistanceToGoal { get; private set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        [JsonProperty("status")]
        public RecipeStatus Status { get; private set; }

        /// <summary>
        /// Gets or sets the current step.
        /// </summary>
        /// <value>The current step.</value>
        [JsonProperty("current_step")]
        public int CurrentStep { get; private set; }

        /// <summary>
        /// Gets or sets the recipe.
        /// </summary>
        /// <value>The recipe.</value>
        [JsonProperty("recipe")]
        public string Recipe { get; private set; }

        /// <summary>
        /// Gets or sets the place.
        /// </summary>
        /// <value>The place.</value>
        [JsonProperty("place")]
        public string Place { get; private set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Status.ToString();
        }
        #endregion
    }
}
