﻿using System;
using System.Collections.Generic;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Recipe.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List{Clearpath.Web.Services.Hummingbird.Step}" />
    public class Recipe : List<Step>, INimbusObject
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid? Id { get; private set; }
    }
}
