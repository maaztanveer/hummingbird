﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Step
    /// </summary>
    [JsonObject]
    public class Step : INimbusObject
    {
        #region Initialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Step"/> class.
        /// </summary>
        public Step()
        {
            Attributes = new Dictionary<string, object>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets whether or not this step is interruptable.
        /// </summary>
        /// <value>The status.</value>
        [JsonProperty("status")]
        public bool Interruptable { get; set; }

        /// <summary>
        /// Gets or sets the type of the step.
        /// </summary>
        /// <value>The type of the step.</value>
        [JsonProperty("step_type")]
        public string StepType { get; set; }

        /// <summary>
        /// Gets or sets the attributes.
        /// </summary>
        /// <value>The attributes.</value>
        [JsonProperty("attributes")]
        public Dictionary<string, object> Attributes { get; set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return StepType;
        }
        #endregion
    }
}
