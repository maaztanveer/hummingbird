﻿using System;
using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Class Payload.
    /// </summary>
    public class Payload : INimbusObject
    {
        #region Initialization
        /// <summary>
        /// Creates the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Payload.</returns>
        public static Payload Create(RestProxy proxy, string name=null)
        {
            var payload = new Payload() { Name = name };
            return proxy.Post<Payload>(Apis.Payloads, payload);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Name ?? Id.ToString();
        }
        #endregion

        #region Operators
        /// <summary>
        /// Performs an implicit conversion from <see cref="Payload"/> to <see cref="System.String"/>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator string(Payload value)
        {
            return value.Id.ToString();
        }
        #endregion
    }
}
