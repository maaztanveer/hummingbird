﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// A Nimbus object
    /// </summary>
    public interface INimbusObject
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        Guid? Id { get; }
    }
}
