﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Task
    /// </summary>
    [JsonObject]
    public class Task : INimbusObject
    {
        #region Initialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        public Task()
        {
            Description = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="Place">The place.</param>
        /// <param name="payload">The payload.</param>
        public Task(TaskType type, Guid place, Guid? payload=null) : this()
        {
            TaskType = type;
            Place = place;
            Payload = payload;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="place">The place.</param>
        /// <param name="Payload">The payload.</param>
        public Task(TaskType type, string place, Guid? payload=null) : this()
        {
            TaskType = type;
            Lookup.Instance.Place(place);
            Payload = payload;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the execution time.
        /// </summary>
        /// <value>The execution time.</value>
        [JsonProperty("execution_time")]
        [JsonConverter(typeof(DoubleTimeSpanConverter))]
        public TimeSpan ExecutionTime { get; private set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [JsonProperty("created")]
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets or sets the execution start.
        /// </summary>
        /// <value>The execution start.</value>
        [JsonProperty("execution_start")]
        public DateTime? ExecutionStart { get; private set; }

        /// <summary>
        /// Gets or sets the execution end.
        /// </summary>
        /// <value>The execution end.</value>
        [JsonProperty("execution_end")]
        public DateTime? ExecutionEnd { get; private set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        [JsonProperty("status")]
        public TaskStatus Status { get; private set; }

        /// <summary>
        /// Gets or sets the type of the task.
        /// </summary>
        /// <value>The type of the task.</value>
        [JsonProperty("task_type")]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// Gets or sets the place.
        /// </summary>
        /// <value>The place.</value>
        [JsonProperty("place")]
        public Guid Place { get; set; }

        /// <summary>
        /// Gets or sets the payload.
        /// </summary>
        /// <value>The payload.</value>
        [JsonProperty("payload")]
        public Guid? Payload { get; set; }

        /// <summary>
        /// Gets or sets the new payload.
        /// </summary>
        /// <value>The new payload.</value>
        [JsonProperty("new_payload")]
        public Guid? NewPayload { get; set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return string.Format("{0} at {1}", TaskType, Place);
        }
        #endregion
    }
}
