﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Mission
    /// </summary>
    [JsonObject]
    public class Mission : INimbusObject
    {
        #region Initialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Mission"/> class.
        /// </summary>
        public Mission()
        {
            Description = string.Empty;
            Tasks = new List<Task>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Mission"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Mission(string name) : this()
        {
            Name = name;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the execution time.
        /// </summary>
        /// <value>The execution time.</value>
        [JsonProperty("execution_time")]
        [JsonConverter(typeof(DoubleTimeSpanConverter))]
        public TimeSpan ExecutionTime { get; private set; }

        /// <summary>
        /// Gets or sets the tasks.
        /// </summary>
        /// <value>The tasks.</value>
        [JsonProperty("tasks")]
        public List<Task> Tasks { get; private set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [JsonProperty("created")]
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets or sets the execution start.
        /// </summary>
        /// <value>The execution start.</value>
        [JsonProperty("execution_start")]
        public DateTime? ExecutionStart { get; private set; }

        /// <summary>
        /// Gets or sets the execution end.
        /// </summary>
        /// <value>The execution end.</value>
        [JsonProperty("execution_end")]
        public DateTime? ExecutionEnd { get; private set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets the mission status.
        /// </summary>
        /// <value>The mission status.</value>
        [JsonProperty("mission_status")]
        public MissionStatus MissionStatus { get; private set; }

        /// <summary>
        /// Gets or sets the current task.
        /// </summary>
        /// <value>The current task.</value>
        [JsonProperty("current_task")]
        public int CurrentTask { get; private set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>
        [JsonProperty("priority")]
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Mission"/> is finalized.
        /// </summary>
        /// <value><c>true</c> if finalized; otherwise, <c>false</c>.</value>
        [JsonProperty("finalized")]
        public bool Finalized { get; set; }

        /// <summary>
        /// Gets the result text.
        /// </summary>
        /// <value>The result text.</value>
        [JsonProperty("result_text")]
        public string ResultText { get; private set; }

        /// <summary>
        /// Gets the assigned robot.
        /// </summary>
        /// <value>The assigned robot.</value>
        [JsonProperty("assigned_robot")]
        public string AssignedRobot { get; private set; }

        /// <summary>
        /// Gets or sets the force robot.
        /// </summary>
        /// <value>The force robot.</value>
        [JsonProperty("force_robot")]
        public string ForceRobot { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Enqueues this mission.
        /// </summary>
        public void Enqueue(RestProxy proxy=null)
        {
            proxy = proxy ?? RestProxy.Instance;
            var result = proxy.Post<Mission>(Apis.Missions, this);
        }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Name ?? Description ?? Id.ToString();
        }
        #endregion
    }
}
