﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Map
    /// </summary>
    [JsonObject]
    public class Map : INimbusObject
    {
        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [JsonProperty("created")]
        public DateTime Created { get; internal set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets whether or not the map is active.
        /// </summary>
        /// <value>Active</value>
        [JsonProperty("active")]
        public TaskStatus Active { get; internal set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Name ?? Description ?? Id.ToString();
        }
        #endregion
    }
}
