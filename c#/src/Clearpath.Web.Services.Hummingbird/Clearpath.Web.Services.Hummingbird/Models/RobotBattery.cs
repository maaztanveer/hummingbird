﻿using System;
using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot Battery
    /// </summary>
    public class RobotBattery
    {
        #region Properties
        /// <summary>
        /// Gets the charging status.
        /// </summary>
        /// <value>The charging status.</value>
        [JsonProperty("charging_status")]
        public ChargingStatus ChargingStatus { get; private set; }

        /// <summary>
        /// Gets the consumption rate.
        /// </summary>
        /// <value>The consumption rate.</value>
        [JsonProperty("consumption_rate")]
        public double ConsumptionRate { get; private set; }

        /// <summary>
        /// Gets the full charge capacity.
        /// </summary>
        /// <value>The full charge capacity.</value>
        [JsonProperty("full_charge_capacity")]
        public double FullChargeCapacity { get; private set; }

        /// <summary>
        /// Gets the remaining charge capacity.
        /// </summary>
        /// <value>The remaining charge capacity.</value>
        [JsonProperty("remaining_charge_capacity")]
        public double RemainingChargeCapacity { get; private set; }

        /// <summary>
        /// Gets the time remaining.
        /// </summary>
        /// <value>The time remaining.</value>
        [JsonProperty("time_remaining")]
        public TimeSpan TimeRemaining { get; private set; }

        /// <summary>
        /// Gets the temperature.
        /// </summary>
        /// <value>The temperature.</value>
        [JsonProperty("temperature")]
        public double Temperature { get; private set; }

        /// <summary>
        /// Gets the pack voltage.
        /// </summary>
        /// <value>The pack voltage.</value>
        [JsonProperty("pack_voltage")]
        public double PackVoltage { get; private set; }

        /// <summary>
        /// Gets the pack current.
        /// </summary>
        /// <value>The pack current.</value>
        [JsonProperty("pack_current")]
        public double PackCurrent { get; private set; }

        /// <summary>
        /// Gets the input power voltage.
        /// </summary>
        /// <value>The input power voltage.</value>
        [JsonProperty("input_power_voltage")]
        public double InputPowerVoltage { get; private set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return string.Format("{0}% {1}", Math.Round(RemainingChargeCapacity / FullChargeCapacity), ChargingStatus);
        }
        #endregion
    }
}
