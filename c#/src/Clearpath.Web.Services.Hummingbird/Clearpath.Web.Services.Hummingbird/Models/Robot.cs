﻿using System;
using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot
    /// </summary>
    [JsonObject]
    public class Robot : INimbusObject
    {
        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the battery.
        /// </summary>
        /// <value>The battery.</value>
        [JsonProperty("battery")]
        public RobotBattery Battery { get; private set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [JsonProperty("robot_type")]
        public RobotType RobotType { get; private set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        [JsonProperty("robot_status")]
        public RobotStatus RobotStatus { get; private set; }

        /// <summary>
        /// Gets or sets the state of the execution.
        /// </summary>
        /// <value>The state of the execution.</value>
        [JsonProperty("execution_state")]
        public RecipeExecution ExecutionState { get; private set; }

        /// <summary>
        /// Gets or sets the payload.
        /// </summary>
        /// <value>The payload.</value>
        [JsonProperty("payload")]
        public Payload Payload { get; private set; }

        /// <summary>
        /// Gets or sets the activity.
        /// </summary>
        /// <value>The activity.</value>
        [JsonProperty("activity")]
        public Activity Activity { get; private set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [JsonProperty("created")]
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the namespace.
        /// </summary>
        /// <value>The namespace.</value>
        [JsonProperty("namespace")]
        public string Namespace { get; private set; }

        /// <summary>
        /// Gets or sets the hostname.
        /// </summary>
        /// <value>The hostname.</value>
        [JsonProperty("hostname")]
        public string Hostname { get; private set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; private set; }

        /// <summary>
        /// Gets the last startup.
        /// </summary>
        /// <value>The last startup.</value>
        [JsonProperty("last_startup")]
        public DateTime LastStartup { get; private set; }

        /// <summary>
        /// Gets or sets the appliance ip.
        /// </summary>
        /// <value>The appliance ip.</value>
        [JsonProperty("appliance_ip")]
        public string ApplianceIP { get; private set; }

        /// <summary>
        /// Gets or sets the fallback recipe.
        /// </summary>
        /// <value>The fallback recipe.</value>
        [JsonProperty("fallback_recipe")]
        public string FallbackRecipe { get; private set; }

        /// <summary>
        /// Gets or sets the place.
        /// </summary>
        /// <value>The place.</value>
        [JsonProperty("place")]
        public string Place { get; private set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
