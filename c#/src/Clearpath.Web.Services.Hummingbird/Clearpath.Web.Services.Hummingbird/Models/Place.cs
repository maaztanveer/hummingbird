﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Place
    /// </summary>
    [JsonObject]
    public class Place : INimbusObject
    {
        #region Initialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Place"/> class.
        /// </summary>
        public Place()
        {
            Recipes = new Dictionary<string, Guid?>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [JsonProperty("id")]
        public Guid? Id { get; private set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [JsonProperty("created")]
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the recipes.
        /// </summary>
        /// <value>The recipes.</value>
        [JsonProperty("recipes")]
        public Dictionary<string, Guid?> Recipes { get; private set; }

        /// <summary>
        /// Gets whether or not this place is enabled.
        /// </summary>
        /// <value>Enabled.</value>
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the queue.
        /// </summary>
        /// <value>The queue.</value>
        public Step[] Queue { get; set; }

        /// <summary>
        /// Gets or sets the zone.
        /// </summary>
        /// <value>The zone.</value>
        [JsonProperty("zone")]
        public string Zone { get; set; }

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>The group.</value>
        [JsonProperty("group")]
        public string Group { get; set; }

        /// <summary>
        /// Gets or sets the exit recipe.
        /// </summary>
        /// <value>The exit recipe.</value>
        [JsonProperty("exit_recipe")]
        public Guid? ExitRecipe { get; set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return Name ?? Description ?? Id.ToString();
        }
        #endregion

        #region Operators
        /// <summary>
        /// Performs an implicit conversion from <see cref="Place"/> to <see cref="System.Nullable{Guid}"/>.
        /// </summary>
        /// <param name="place">The place.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator Guid?(Place place)
        {
            return place.Id;
        }
        #endregion
    }
}
