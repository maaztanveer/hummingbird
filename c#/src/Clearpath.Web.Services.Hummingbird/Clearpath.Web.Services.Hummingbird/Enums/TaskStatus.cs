﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Task Status
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TaskStatus
    {
        [EnumMember(Value = "CANCELLED")]
        Cancelled,
        [EnumMember(Value = "EXECUTING")]
        Executing,
        [EnumMember(Value = "FAILED")]
        Failed,
        [EnumMember(Value = "PAUSED")]
        Paused,
        [EnumMember(Value = "QUEUED")]
        Queued,
        [EnumMember(Value = "SUCCEEDED")]
        Succeeded
    }
}
