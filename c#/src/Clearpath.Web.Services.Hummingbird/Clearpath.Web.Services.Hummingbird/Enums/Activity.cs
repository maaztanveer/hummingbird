﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Nimbus Activity
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Activity
    {
        Missioning,
        Maintenance,
        Parking,
        Charging
    }
}
