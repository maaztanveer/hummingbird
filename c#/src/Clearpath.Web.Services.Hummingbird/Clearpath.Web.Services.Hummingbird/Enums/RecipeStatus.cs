﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Recipe Execution Status
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RecipeStatus
    {
        [EnumMember(Value = "CANCELLED")]
        Cancelled,
        [EnumMember(Value = "CANCELLING")]
        Cancelling,
        [EnumMember(Value = "EXECUTING")]
        Executing,
        [EnumMember(Value = "EXITING")]
        Exiting,
        [EnumMember(Value = "FAILED")]
        Failed,
        [EnumMember(Value = "INITIAL")]
        Initial,
        [EnumMember(Value = "PARKED")]
        Parked,
        [EnumMember(Value = "QUEUED")]
        Queued,
        [EnumMember(Value = "SUCCEEDED")]
        Succeeded
    }
}
