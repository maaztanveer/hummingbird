﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Nimbus APIs
    /// </summary>
    public static class Apis
    {
        // Public APIs
        public readonly static string Robots = "robots";
        public readonly static string Missions = "missions";
        public readonly static string RobotExceptions = "exceptions/robots";
        public readonly static string NimbusExceptions = "exceptions/nimbus";
        public readonly static string Interlocks = "interlocks";
        public readonly static string MissionOperations = "missions/operations";
        public readonly static string RobotFootprints = "robots/footprints";
        public readonly static string RobotOperations = "robots/operations";
        public readonly static string SystemVersions = "system/versions";
        public readonly static string SystemParameters = "system/parameters";
        public readonly static string SystemAgents = "system/agents";
        public readonly static string Payloads = "payloads";

        // private APIs
        public readonly static string Maps = "maps";
        public readonly static string Places = "places";
        public readonly static string Markers = "maps/{map}/markers";
        public readonly static string Recipes = "maps/{map}/recipes";
        public readonly static string Zones = "maps/{map}/zones";

        // Kiosk APIs
        public readonly static string Kiosks = "kiosks";
        public readonly static string KioskMissionTemplates = "mission_templates";
        public readonly static string KioskMissions = "missions";
    }
}
