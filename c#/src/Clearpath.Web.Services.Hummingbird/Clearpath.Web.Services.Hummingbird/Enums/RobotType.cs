﻿using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot Type
    /// </summary>
    public class RobotType
    {
        #region Properties
        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        [JsonProperty("description")]
        public string Description { get; private set; }

        /// <summary>
        /// Gets the name of the model.
        /// </summary>
        /// <value>The name of the model.</value>
        [JsonProperty("model_name")]
        public string ModelName { get; private set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return ModelName;
        }
        #endregion
    }
}
