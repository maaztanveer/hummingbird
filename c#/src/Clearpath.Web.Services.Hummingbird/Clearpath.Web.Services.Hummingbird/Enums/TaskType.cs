﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Task Type
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TaskType
    {
        [EnumMember(Value = "CHARGE")]
        Charge,
        [EnumMember(Value = "LOAD")]
        Load,
        [EnumMember(Value = "MOVE")]
        Move,
        [EnumMember(Value = "UNLOAD")]
        Unload,
        [EnumMember(Value = "TRANSPORT")]
        Transport,
        [EnumMember(Value = "WORK_IN_PLACE")]
        WorkInPlace,
        [EnumMember(Value = "MUTATE")]
        Mutate
    }
}
