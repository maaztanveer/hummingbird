﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Sub-system State
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SubSystemState
    {
        [EnumMember(Value = "APPLIANCE")]
        Appliance,
        [EnumMember(Value = "BLOCKED")]
        Blocked,
        [EnumMember(Value = "CHARGING")]
        Charging,
        [EnumMember(Value = "E_STOP")]
        EStop,
        [EnumMember(Value = "FAILED_TASK")]
        FailedTask,
        [EnumMember(Value = "INTERLOCK")]
        Interlock,
        [EnumMember(Value = "LOST")]
        Lost,
        [EnumMember(Value = "MAINTENANCE")]
        Maintenance,
        [EnumMember(Value = "MANUAL")]
        Manual,
        [EnumMember(Value = "NO_HEARTBEAT")]
        NoHeartbeat,
        [EnumMember(Value = "PAUSED")]
        Paused,
        [EnumMember(Value = "QUEUED")]
        Queued,
        [EnumMember(Value = "RUNNING")]
        Running,
        [EnumMember(Value = "SAFETY_STOP")]
        SafetyStop,
        [EnumMember(Value = "SLOW")]
        Slow,
        [EnumMember(Value = "STARVED")]
        Starved,
        [EnumMember(Value = "UNAVAILABLE")]
        Unavailable
    }
}
