﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// System State
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SystemState
    {
        [EnumMember(Value = "OFFLINE")]
        Offline,
        [EnumMember(Value = "RUN")]
        Run,
        [EnumMember(Value = "STOP")]
        Stop,
        [EnumMember(Value = "SYS")]
        Sys,
        [EnumMember(Value = "WAIT")]
        Wait
    }
}
