﻿using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot Status
    /// </summary>
    public class RobotStatus
    {
        #region Properties
        /// <summary>
        /// Gets the state of the system.
        /// </summary>
        /// <value>The state of the system.</value>
        [JsonProperty("system_state")]
        public SystemState SystemState { get; private set; }

        /// <summary>
        /// Gets the state of the sub system.
        /// </summary>
        /// <value>The state of the sub system.</value>
        [JsonProperty("sub_system_state")]
        public SubSystemState SubSystemState { get; private set; }
        #endregion

        #region Method Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return SystemState + "/" + SubSystemState;
        }
        #endregion
    }
}
