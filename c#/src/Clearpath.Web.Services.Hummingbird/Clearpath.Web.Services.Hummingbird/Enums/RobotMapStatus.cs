﻿using Newtonsoft.Json;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot Map Status
    /// </summary>
    public class RobotMapStatus
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="RobotMapStatus"/> is activated.
        /// </summary>
        /// <value><c>true</c> if activated; otherwise, <c>false</c>.</value>
        [JsonProperty("activated")]
        public bool Activated { get; set; }


        /// <summary>
        /// Gets or sets the transfer percentage.
        /// </summary>
        /// <value>The transfer percentage.</value>
        [JsonProperty("transfer_percentage")]
        public int TransferPercentage { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// </summary>
        /// <value>The map.</value>
        [JsonProperty("map")]
        public string Map { get; set; }
        #endregion
    }
}
