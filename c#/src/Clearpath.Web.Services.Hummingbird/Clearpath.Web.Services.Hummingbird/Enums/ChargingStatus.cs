﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    /// Robot Charging Status
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ChargingStatus
    {
        Charging,
        Discharging,
        Full,
        NotCharging
    }
}
