﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Clearpath.Web.Services.Hummingbird
{
    /// <summary>
    ///Mission Status
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MissionStatus
    {
        [EnumMember(Value = "ASSIGNED")]
        Assigned,
        [EnumMember(Value = "CANCELLED")]
        Cancelled,
        [EnumMember(Value = "CANCELLING")]
        Cancelling,
        [EnumMember(Value = "EXECUTING")]
        Executing,
        [EnumMember(Value = "FAILED")]
        Failed,
        [EnumMember(Value = "PAUSED")]
        Paused,
        [EnumMember(Value = "QUEUED")]
        Queued,
        [EnumMember(Value = "REASSIGNED")]
        Reassigned,
        [EnumMember(Value = "REVOKED")]
        Revoked,
        [EnumMember(Value = "STARVED")]
        Starved,
        [EnumMember(Value = "SUCCEEDED")]
        Succeeded
    }
}
